from pymongo import MongoClient


def drop_collection(db, coll):
  db.coll.drop()

def insert_objects(db, countries):
  ctrs = db.countries
  ctrs.insert_many(countries)

def group_by_county_name(db):
  cursor = db.countries.aggregate([
    {
      "$group": {
        "_id": { "country":  "$country" },
        "mcc": { "$addToSet": "$mcc" }
      }
    }
  ])

  return list(cursor)
  

if __name__ == '__main__':
  client = MongoClient('localhost')
  db = client.work

  countries = [
    { "mcc": 404, "country": "India" },
    { "mcc": 405, "country": "India" },
    { "mcc": 412, "country": "Afghanistan" }
  ]

  drop_collection(db, "countries")

  insert_objects(db, countries)
  
  print(group_by_county_name(db))

  
  client.close()
